//
//  main.m
//  maptest
//
//  Created by Daniel Phillips on 03/05/2013.
//  Copyright (c) 2013 djp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
