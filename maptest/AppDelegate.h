//
//  AppDelegate.h
//  maptest
//
//  Created by Daniel Phillips on 03/05/2013.
//  Copyright (c) 2013 djp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
